package info.leetcode.answer.Link.moveOrInsert;

/**
 * @author xiaolong
 * @version 1.0
 * @description: TODO
 * @date 2024/3/14 9:48
 */
public class RemoveElements {

    class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }


    public void main(String[] args) {
        ListNode listNode = new ListNode();
        ListNode result = removeElements(listNode, 6);
    }

    private ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return null;
        }
        ListNode listNode = removeElements(head.next, val);
        if (head.val == val) {
            return listNode;
        }
        head.next = listNode;
        return head;
    }
}
