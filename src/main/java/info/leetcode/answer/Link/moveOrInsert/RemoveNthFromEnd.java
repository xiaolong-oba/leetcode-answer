package info.leetcode.answer.Link.moveOrInsert;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 删除链表的倒数第 N 个结点
 * @date 2024/3/18 10:54
 */
public class RemoveNthFromEnd {

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    Integer size = 1;
    Integer target;

    public ListNode removeNthFromEnd(ListNode head, int n) {
        size++;
        if (head.next == null) {
            target = size - n;
            size--;
            if (target == size) {
                head = head.next;
            }
            return head;
        }
        head.next = removeNthFromEnd(head.next, n);
        size--;
        if (target == size) {
            head = head.next;
            return head;
        }
        return head;
    }

}
