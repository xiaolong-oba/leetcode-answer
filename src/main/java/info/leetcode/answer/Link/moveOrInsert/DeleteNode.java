package info.leetcode.answer.Link.moveOrInsert;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 删除链表中的节点
 * @date 2024/3/15 15:26
 */
public class DeleteNode {

    class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

}
