package info.leetcode.answer.Link.moveOrInsert;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 删除排序链表中的重复元素
 * @date 2024/3/19 14:42
 */
public class DeleteDuplicates {
     class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        //1,1,2
        if (head ==null ||head.next == null){
            return  head;
        }
        ListNode listNode = deleteDuplicates(head.next);
        if (listNode.val == head.val){
            return  listNode;
        }
        head.next = listNode;
        return  head ;
    }
}
