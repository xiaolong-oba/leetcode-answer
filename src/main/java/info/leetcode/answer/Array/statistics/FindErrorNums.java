package info.leetcode.answer.Array.statistics;

import java.util.Arrays;

/**
 * @Classname FindErrorNums
 * @Description 错误的集合
 * @Date 2024/1/15 15:09
 * @Created by 晓龙Oba
 */
public class FindErrorNums {
    public static void main(String[] args) {
        int[] nums = {1, 2, 2, 4};
        int[] result = finderrorNums(nums);
    }

    private static int[] finderrorNums(int[] nums) {
        int[] result = new int[2];
        Arrays.sort(nums);
        int sum = nums[0];
        int realSum = 1;
        for (int i = 1; i < nums.length; i++) {
            sum += nums[i];
            if (nums[i] == nums[i - 1]) {
                result[0] = nums[i];
            }
            realSum += i + 1;
        }
        result[1] = result[0] - (sum - realSum);
        return result;
    }
}
