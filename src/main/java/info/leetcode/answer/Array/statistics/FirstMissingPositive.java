package info.leetcode.answer.Array.statistics;

public class FirstMissingPositive {
    public static void main(String[] args) {
        int[] nums = {1, 2, 0};
        int result = firstMissingPositive(nums);
        System.out.println(result);
    }

    private static int firstMissingPositive(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] <= 0) {
                nums[i] = nums.length + 1;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            int currentValue = Math.abs(nums[i]);
            if (currentValue > 0 && currentValue <= nums.length) {
                int targetIndex = currentValue - 1;
                nums[targetIndex] = 0 - Math.abs(nums[targetIndex]);
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                return i + 1;
            }
        }
        return nums.length + 1;

    }
}
