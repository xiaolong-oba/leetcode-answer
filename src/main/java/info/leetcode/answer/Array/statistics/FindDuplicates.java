package info.leetcode.answer.Array.statistics;

import java.util.ArrayList;
import java.util.List;

public class FindDuplicates {

    public static void main(String[] args) {
        int[] nums = {10, 2, 5, 10, 9, 1, 1, 4, 3, 7};
        List<Integer> result = findDuplicates(nums);
        System.out.println(result);
    }

    private static List<Integer> findDuplicates(int[] nums) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            int targetIndex = Math.abs(nums[i]) - 1;
            nums[targetIndex] = 0 - nums[targetIndex];
            if (nums[targetIndex] > 0) {
                result.add(Math.abs(nums[i]));
            }
        }
        return result;
    }
}
