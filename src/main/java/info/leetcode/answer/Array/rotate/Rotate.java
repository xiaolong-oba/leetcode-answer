package info.leetcode.answer.Array.rotate;

/**
 * @Classname Rotate
 * @Description 旋转数组
 * @Date 2024/1/8 17:24
 * @Created by 晓龙Oba
 */
public class Rotate {
    public static void main(String[] args) {
        int[] nums = {-1, 2};
        int k = 3;
        rotate(nums, k);

    }

    private static void rotate(int[] nums, int k) {
        k = k % nums.length;
        for (int i = 0; i < nums.length / 2; i++) {
            nums[i] ^= nums[nums.length - 1 - i];
            nums[nums.length - 1 - i] ^= nums[i];
            nums[i] ^= nums[nums.length - 1 - i];
        }
        System.out.println(nums);
        for (int i = 0; i < k / 2; i++) {
            nums[i] ^= nums[k - 1 - i];
            nums[k - 1 - i] ^= nums[i];
            nums[i] ^= nums[k - 1 - i];
        }
        System.out.println(nums);
        int flag = 1;
        for (int i = k; i < (nums.length + k) / 2; i++) {
            nums[i] ^= nums[nums.length - flag];
            nums[nums.length - flag] ^= nums[i];
            nums[i] ^= nums[nums.length - flag];
            flag++;
        }
        System.out.println(nums);
    }
}
