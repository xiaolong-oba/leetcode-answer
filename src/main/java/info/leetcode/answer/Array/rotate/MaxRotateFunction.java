package info.leetcode.answer.Array.rotate;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname MaxRotateFunction
 * @Description 旋转函数
 * @Date 2024/1/11 9:35
 * @Created by 晓龙Oba
 */
public class MaxRotateFunction {
    public static void main(String[] args) {
        int[] nums = {4, 3, 2, 6};
        int result = maxRotateFunction(nums);
        System.out.println(result);
    }

    private static int maxRotateFunction(int[] nums) {
        Integer total = 0;
        Integer sum = 0;
        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            total += nums[i] * i;
            sum += nums[i];
        }
        resultList.add(total);
        Integer result = total;
        for (int i = 1; i < nums.length; i++) {
            int current = resultList.get(i - 1) + sum - (nums.length * (nums[nums.length - i]));
            result = result > current ? result : current;
            resultList.add(current);
        }
        return result;
    }
}
