package info.leetcode.answer.Array.TwoDimensionalArray.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 杨辉三角 II
 * @date 2024/2/5 15:41
 */
public class GetRow {
    public static void main(String[] args) {
        int rowIndex = 3 ;
        List<Integer> result = getRow(rowIndex);
        System.out.println(result);
    }

    private static List<Integer> getRow(int rowIndex) {
        rowIndex++;
        List<Integer> list = new ArrayList<>();
        // 先把C1 放进
        list.add(1);
        for (int i = 1; i < rowIndex; i++) {
            List<Integer> temp = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                int a = j == 0 ? 0 : list.get(j - 1);
                int b = j == i ? 0 : list.get(j);
                temp.add(a + b);
            }
            list = temp ;
        }
        return list ;
    }

}
