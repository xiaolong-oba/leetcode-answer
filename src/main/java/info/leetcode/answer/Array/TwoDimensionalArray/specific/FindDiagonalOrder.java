package info.leetcode.answer.Array.TwoDimensionalArray.specific;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 对角线遍历
 * @date 2024/2/26 11:01
 */
public class FindDiagonalOrder {
    public static void main(String[] args) {
        int[][] mat = {{2, 3}};
        int[] result = findDiagonalOrder(mat);
        System.out.println(result);
    }

    private static int[] findDiagonalOrder(int[][] mat) {
        int[] result = new int[mat.length * mat[0].length];
        int index = 0;
        for (int i = 0; i < result.length; i++) {
            // 总和守恒  奇偶判断方向
            //指针
            for (int j = i; j >= 0; j--) {
                if (i % 2 == 0) {
                    if (j >= mat.length || i - j >= mat[0].length) {
                        continue;
                    }
                    result[index] = mat[j][i - j];
                } else {
                    if (j >= mat[0].length || i - j >= mat.length) {
                        continue;
                    }
                    result[index] = mat[i - j][j];
                }
                index++;
            }
            if (index == result.length) {
                break;
            }
        }

        return result;
    }
}
