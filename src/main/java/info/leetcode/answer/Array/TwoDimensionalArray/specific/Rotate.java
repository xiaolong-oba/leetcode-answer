package info.leetcode.answer.Array.TwoDimensionalArray.specific;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 旋转图像
 * @date 2024/2/28 16:18
 */
public class Rotate {
    public static void main(String[] args) {
        int[][] matrix = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}} ;
        matrix = rotate(matrix);
        System.out.println(matrix);
    }

    private static int[][] rotate(int[][] matrix) {
        // 对向翻转
        for (int i = 0; i < matrix.length / 2; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] ^= matrix[(matrix.length - 1) - i][j];
                matrix[(matrix.length - 1) - i][j] ^= matrix[i][j];
                matrix[i][j] ^= matrix[(matrix.length - 1) - i][j];
            }
        }
        // 对角翻转
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i + 1; j < matrix[i].length; j++) {
                matrix[i][j] ^= matrix[j][i] ;
                matrix[j][i] ^= matrix[i][j];
                matrix[i][j] ^= matrix[j][i] ;
            }
        }
        return matrix;
    }
}
