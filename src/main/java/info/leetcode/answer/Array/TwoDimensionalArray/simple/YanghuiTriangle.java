package info.leetcode.answer.Array.TwoDimensionalArray.simple;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 杨辉三角
 * @date 2024/2/1 15:37
 */
public class YanghuiTriangle {
    public static void main(String[] args) {
        int numRows = 6;
        List<List<Integer>> result = generate(numRows);
        System.out.println(result);
    }

    private static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        // 先把C1 放进去
        List<Integer> initArr = new ArrayList<>();
        initArr.add(1);
        result.add(initArr);
        for (int i = 1; i < numRows; i++) {
            List<Integer> temp = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                List<Integer> integers = result.get(i - 1);
                int a = j == 0 ? 0 : integers.get(j - 1);
                int b = j == i ? 0 : integers.get(j);
                temp.add(a + b);
            }
            result.add(temp);
        }
        return result;
    }
}
