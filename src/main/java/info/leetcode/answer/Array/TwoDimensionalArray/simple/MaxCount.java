package info.leetcode.answer.Array.TwoDimensionalArray.simple;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 区间加法 II
 * @date 2024/2/20 9:53
 */
public class MaxCount {
    public static void main(String[] args) {
        int m = 3;
        int n = 3;
        int[][] ops = {{2, 2}, {3, 3}};
        int result = maxCount(m, n, ops);
        System.out.println(result);
    }

    private static int maxCount(int m, int n, int[][] ops) {
        // 求集合的最小交集
        int transverse = m ;
        int direction = n ;
        for (int[] op : ops) {
            transverse = transverse < op[0] ? transverse :op[0];
            direction = direction < op[1] ? direction :op[1];
        }
        return transverse * direction;
    }
}
