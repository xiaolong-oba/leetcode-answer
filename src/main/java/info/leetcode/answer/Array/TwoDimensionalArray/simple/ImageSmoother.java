package info.leetcode.answer.Array.TwoDimensionalArray.simple;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 图片平滑器
 * @date 2024/2/19 15:06
 */
public class ImageSmoother {
    public static void main(String[] args) {
        int[][] img = {{2,3,4},{5,6,7},{8,9,10},{11,12,13},{14,15,16}};
        int[][] result = imageSmoother(img);
        System.out.println(result);
    }

    private static int[][] imageSmoother(int[][] img) {
        int[][] result = new int[img.length][img[0].length];
        for (int i = 0; i < img.length; i++) {
            for (int j = 0; j < img[i].length; j++) {
                int total = 0 ;
                int sum = 0 ;
                for (int k = -1; k <2 ; k++) {
                    for (int l =-1; l < 2; l++) {
                        try {
                            sum += img[i+k][j+l];
                            total++ ;
                        }catch (ArrayIndexOutOfBoundsException e){
                            continue;
                        }
                    }
                }
                result[i][j] = sum /total ;
            }
        }
        return result ;
    }
}
