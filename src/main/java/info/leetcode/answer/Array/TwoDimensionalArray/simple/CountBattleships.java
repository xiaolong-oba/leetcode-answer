package info.leetcode.answer.Array.TwoDimensionalArray.simple;

import javax.swing.*;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 甲板上的战舰
 * @date 2024/2/21 9:44
 */
public class CountBattleships {
    public static void main(String[] args) {
        char[][] board = {{'X', '.', '.', 'X'}, {'.', '.', '.', 'X'}, {'.', '.', '.', 'X'}};
        int result = countBattleships(board);
        System.out.println(result);
    }

    private static int countBattleships(char[][] board) {
        int result = 0;

        for (int i = 0; i < board.length; i++) {
            char[] chars = board[i];
            for (int j = 0; j < chars.length; j++) {
                if ('X' == chars[j]) {
                    result++;
                    char tempA = j - 1 >= 0 ? chars[j - 1] : '.';
                    char tempB = i - 1 >= 0 ? board[i - 1][j] : '.';
                    if (tempA == 'X' || tempB == 'X') {
                        result--;
                    }

                }
            }
        }

        return result;
    }
}
