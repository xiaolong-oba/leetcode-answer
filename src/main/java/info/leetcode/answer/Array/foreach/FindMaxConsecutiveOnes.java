package info.leetcode.answer.Array.foreach;


public class FindMaxConsecutiveOnes {

    public static void main(String[] args) {
        int[] nums = {1, 1, 0, 1, 1, 1};
        int result = findMaxConsecutiveOnes(nums);
        System.out.println(result);
    }

    private static int findMaxConsecutiveOnes(int[] nums) {
        int max = 0;
        int curretValue = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                curretValue++;
            } else {
                max = max > curretValue ? max : curretValue;
                curretValue = 0;
            }
        }
        return max > curretValue ? max : curretValue;
    }
}
