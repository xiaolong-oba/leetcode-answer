package info.leetcode.answer.Array.foreach;

import java.util.Arrays;

/**
 * @Classname MinMoves
 * @Description 最小操作次数使数组元素相等
 * @Date 2023/12/26 14:44
 * @Created by 晓龙Oba
 */
public class MinMoves {
    public static void main(String[] args) {
        int[] nums = {1, 2};
        int result = minMoves(nums);
        System.out.println(result);
    }

    private static int minMoves(int[] nums) {
        int result = 0;
        Arrays.sort(nums);
        for (int i = nums.length - 1; i > 0; i--) {
            result += (nums[i] - nums[0]);
        }
        return result;
    }
}
