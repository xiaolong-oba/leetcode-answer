package info.leetcode.answer.Array.foreach;

import java.util.Arrays;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 三个数的最大乘积
 * @date 2024/1/31 10:30
 */
public class MaximumProduct {
    public static void main(String[] args) {
        int[] nums = {-100,-98,-1,2,3,4};
        int result = maximumProduct(nums);
        System.out.println(result);
    }

    private static int maximumProduct(int[] nums) {
        Arrays.sort(nums);
        int A = nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
        int B = nums[0] * nums[1] * nums[nums.length - 1];
        return A>B?A:B;
    }
}
