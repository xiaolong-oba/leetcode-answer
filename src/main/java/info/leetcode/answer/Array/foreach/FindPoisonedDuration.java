package info.leetcode.answer.Array.foreach;

public class FindPoisonedDuration {
    public static void main(String[] args) {
        int[] timeSeries = {1, 4};
        int duration = 2;
        int result = findPoisonedDuration(timeSeries, duration);
        System.out.println(result);
    }

    private static int findPoisonedDuration(int[] timeSeries, int duration) {
        int result = duration;
        for (int i = 1; i < timeSeries.length; i++) {
            int series = timeSeries[i] - (timeSeries[i - 1] + duration);
            result += series >= 0 ? duration : duration + series ;
        }
        return result;
    }
}
