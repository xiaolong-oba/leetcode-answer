package info.leetcode.answer.Array.changeAndMove;

/**
 * @Classname MoveZeroes
 * @Description 移动零
 * @Date 2024/1/5 16:06
 * @Created by 晓龙Oba
 */
public class MoveZeroes {
    public static void main(String[] args) {
        int[] nums = {1, 0, 0, 3, 12};
        nums = moveZeroes(nums);
        System.out.println(nums);
    }

    private static int[] moveZeroes(int[] nums) {
        int zeroIndex = -1;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                continue;
            } else {
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] != 0) {
                        nums[i] ^= nums[j];
                        nums[j] ^= nums[i];
                        nums[i] ^= nums[j];
                        break;
                    }
                }
            }

        }
        return nums;
    }
}
