package info.leetcode.answer.Array.changeAndMove;

/**
 * @Classname CheckPossibility
 * @Description 非递减数列
 * @Date 2023/12/28 21:27
 * @Created by 晓龙Oba
 */
public class CheckPossibility {
    public static void main(String[] args) {
        int[] nums = {4, 2, 3};
        Boolean result = checkPossibility(nums);
        System.out.println(result);
    }

    private static Boolean checkPossibility(int[] nums) {
        // 记录次数
        int changeTime = 0;
        // 遍历元素
        for (int i = 0; i < nums.length - 1; i++) {
            // 如果已经超过1次,代表不行
            if (changeTime > 1) return false;
            // 判断numsi和numsi+1之间的关系
            if (nums[i] > nums[i + 1]) {
                // 如果是数组第一个元素 直接修改即可
                if (i == 0) nums[i] = nums[i + 1] - 1;
                    // 如果i+1 < i-1 那么只能通过修改i+1进行改变
                else if (nums[i - 1] > nums[i + 1]) nums[i + 1] = nums[i];
                    // 否则修改i的性价比更高
                else nums[i] = nums[i - 1];
                changeTime++;
            }
        }

        return changeTime < 2;
    }
}
