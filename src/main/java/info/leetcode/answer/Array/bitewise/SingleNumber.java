package info.leetcode.answer.Array.bitewise;

/**
 * @Classname SingleNumber
 * @Description 只出现一次的数字
 * @Date 2023/12/25 15:06
 * @Created by 晓龙Oba
 */
public class SingleNumber {
    public static void main(String[] args) {
        int[] nums = {2, 1};
        int result = singleNumber(nums);
        System.out.println(result);
    }

    private static int singleNumber(int[] nums) {
        int result = 0;

        for (int i = 0; i < nums.length; i++) {
            result ^= nums[i];
        }
        return result;
    }
}
