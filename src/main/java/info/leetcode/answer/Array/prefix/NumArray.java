package info.leetcode.answer.Array.prefix;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 区域和检索 - 数组不可变
 * @date 2024/3/4 10:06
 */
public class NumArray {
    private int[] nums;

    public NumArray(int[] nums) {
        this.nums = nums;
    }

    public int sumRange(int left, int right) {
        int result = nums[left];
        for (int i = left + 1; i < right; i++) {
            result += nums[i];
        }
        return result;
    }

    public static void main(String[] args) {
        NumArray numArray = new NumArray(new int[]{-2, 0, 3, -5, 2, -1});
        int result = numArray.sumRange(0, 2);
        System.out.println(result);
    }
}
