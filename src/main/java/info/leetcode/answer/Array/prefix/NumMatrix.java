package info.leetcode.answer.Array.prefix;

/**
 * @author xiaolong
 * @version 1.0
 * @description: 二维区域和检索 - 矩阵不可变
 * @date 2024/3/6 9:39
 */
public class NumMatrix {
    int[][] matrix;

    public NumMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        int result = 0;
        for (int i = row1; i <= row2; i++) {
            for (int j = col1; j <= col2; j++) {
                result += matrix[i][j];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] matrix = {{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}};
        NumMatrix numMatrix = new NumMatrix(matrix);
        int result = numMatrix.sumRegion(2, 1, 4, 3);
        System.out.println(result);
    }
}
