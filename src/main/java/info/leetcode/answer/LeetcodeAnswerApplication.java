package info.leetcode.answer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeetcodeAnswerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeetcodeAnswerApplication.class, args);
    }

}
