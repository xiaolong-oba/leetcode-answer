package info.leetcode.answer.String.Prefix;

public class LongestCommonPrefix {
    public static void main(String[] args) {
        String[] strs = {"ab", "a"};
        String result= longestCommonPrefix(strs);
        System.out.println(result);
    }

    private static String longestCommonPrefix(String[] strs) {
        char[] charArray = strs[0].toCharArray();
        String result = "";
        for (int i = 0; i < charArray.length; i++) {
            for (int j = 0; j < strs.length; j++) {
                if (strs[j].length()<=i||strs[j].toCharArray()[i] != charArray[i]){
                    return result ;
                }
            }
            result += charArray[i] ;
        }
        return result ;
    }
}
