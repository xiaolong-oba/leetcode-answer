package info.leetcode.answer.String.Char;

public class DetectCapitalUse {

    public static void main(String[] args) {
        String word = "aaa";
        Boolean result = detectCapitalUse(word);
        System.out.println(result);
    }

    private static Boolean detectCapitalUse(String word) {
        Integer total = 0;
        char[] charArray = word.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] >= 'a' && charArray[i] <= 'z') {
                total++;
            }
        }

        return total == 0 || total == charArray.length || (charArray[0] >= 'A' && charArray[0] <= 'Z' && total + 1 == charArray.length);
    }
}
