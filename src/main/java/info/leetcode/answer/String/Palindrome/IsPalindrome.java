package info.leetcode.answer.String.Palindrome;

public class IsPalindrome {
    public static void main(String[] args) {
        String paramA = "0P";
        Boolean result = isPalindrome(paramA);
        System.out.println(result);
    }

    private static Boolean isPalindrome(String s) {
            // 去除非字母字符
            char[] charArray = s.toCharArray();
            StringBuffer result = new StringBuffer();
            for (int i = 0; i < charArray.length; i++) {
                if (Character.isLowerCase(charArray[i]) || Character.isUpperCase(charArray[i])|| Character.isDigit(charArray[i])) {
                    result.append(charArray[i]);
                }
            }
            String lowerCase = result.toString().toLowerCase();
            String reverse = result.reverse().toString();
            reverse = reverse.toLowerCase();
            return lowerCase.equals(reverse);
    }
}
