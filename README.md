# 力扣刷题攻略

## 数组类

### 1.只出现一次的数字

- [原题地址：只出现一次的数字](https://leetcode.cn/problems/single-number/)
- [题解：只出现一次的数字](https://leetcode.cn/problems/single-number/solutions/2580171/zhi-chu-xian-yi-ci-de-shu-zi-yi-huo-yun-wph6x/)

### 2. 最小操作次数使数组元素相等

- [原题地址：最小操作次数使数组元素相等](https://leetcode.cn/problems/minimum-moves-to-equal-array-elements/description/)
- [题解：最小操作次数使数组元素相等](https://leetcode.cn/problems/minimum-moves-to-equal-array-elements/submissions/)

### 3. 非递减数组

- [原题地址:非递减数组](https://leetcode.cn/problems/non-decreasing-array/description/)
- [题解:非递减数组](https://leetcode.cn/problems/non-decreasing-array/solutions/2590952/shu-zu-fei-di-jian-shu-lie-ti-jie-by-qin-mnhu/)

### 4. 移动零

- [原题地址:移动零](https://leetcode.cn/problems/move-zeroes/description/)
- [题解:移动零](https://leetcode.cn/problems/move-zeroes/solutions/2592180/shu-zu-yi-dong-ling-by-qing-yi-ruo-su-3boq/)

### 5. 轮转数组

- [原题地址:移动零](https://leetcode.cn/problems/rotate-array/description/)
- [题解:移动零](https://leetcode.cn/problems/rotate-array/solutions/2596821/shu-zu-lun-zhuan-shu-zu-by-qing-yi-ruo-s-ckz1/)

### 6. 旋转函数

- [原题地址:旋转函数](https://leetcode.cn/problems/rotate-function/description/)
- [题解:旋转函数](https://leetcode.cn/problems/rotate-function/solutions/2599638/shu-zu-xuan-zhuan-han-shu-by-qing-yi-ruo-99bw/)

### 7. 错误的集合

- [原题地址:错误的集合](https://leetcode.cn/problems/set-mismatch/description/)
- [题解:错误的集合](https://leetcode.cn/problems/set-mismatch/solutions/2605189/shu-zu-cuo-wu-de-ji-he-by-qing-yi-ruo-su-ue56/)

### 8. 数组的度

- [原题地址:数组的度](https://leetcode.cn/problems/degree-of-an-array/description/)
- [题解:数组的度](https://leetcode.cn/problems/degree-of-an-array/solutions/2606510/shu-zu-shu-zu-de-du-by-qing-yi-ruo-su-u3im/)

### 8. 找到所有数组中消失的数字

- [原题地址:数组的度](https://leetcode.cn/problems/find-all-numbers-disappeared-in-an-array/description/)
- [题解:数组的度](https://leetcode.cn/problems/find-all-numbers-disappeared-in-an-array/solutions/2609047/shu-zu-zhao-dao-suo-you-shu-zu-zhong-xia-k29w/)

### 9. 数组中重复的数据

- [原题地址:数组中重复的数据](https://leetcode.cn/problems/find-all-duplicates-in-an-array/description/)
- [题解:数组中重复的数据](https://leetcode.cn/problems/find-all-duplicates-in-an-array/solutions/2610380/shu-zu-shu-zu-zhong-zhong-fu-de-shu-ju-b-353l/)

### 10. 缺失的第一个正数

- [原题地址:缺失的第一个正数](https://leetcode.cn/problems/first-missing-positive/description/)
- [题解:缺失的第一个正数](https://leetcode.cn/problems/first-missing-positive/solutions/2616445/shu-zu-que-shi-de-di-yi-ge-zheng-shu-by-t49v0/)

### 11. 最大连续 1 的个数

- [原题地址:最大连续 1 的个数](https://leetcode.cn/problems/max-consecutive-ones/description/)
- [题解:最大连续 1 的个数](https://leetcode.cn/problems/max-consecutive-ones/solutions/2617617/shu-zu-zui-da-lian-xu-1de-ge-shu-by-qing-icg5/)

### 12.提莫攻击

- [原题地址: 提莫攻击](https://leetcode.cn/problems/teemo-attacking/description/)
- [题解: 提莫攻击](https://leetcode.cn/problems/teemo-attacking/solutions/2620395/shu-zu-ti-mo-gong-ji-by-qing-yi-ruo-su-0x0i/)

### 13.第三大的数

- [原题地址: 第三大的数](https://leetcode.cn/problems/third-maximum-number/description/)
- [题解: 第三大的数](https://leetcode.cn/problems/third-maximum-number/solutions/2625238/shu-zu-di-san-da-de-shu-by-qing-yi-ruo-s-1io2/)

### 14.三个数的最大乘积

- [原题地址: 三个数的最大乘积](https://leetcode.cn/problems/maximum-product-of-three-numbers/description/)
- [题解: 三个数的最大乘积](https://leetcode.cn/problems/maximum-product-of-three-numbers/solutions/2626396/shu-zu-san-ge-shu-de-zui-da-cheng-ji-by-6e0i2/)

### 15.杨辉三角

- [原题地址: 杨辉三角](https://leetcode.cn/problems/pascals-triangle/description/)
- [题解: 杨辉三角](https://leetcode.cn/problems/pascals-triangle/solutions/2628618/shu-zu-yang-hui-san-jiao-xing-by-qing-yi-013w/)

### 16.杨辉三角 II

- [原题地址: 杨辉三角 II](https://leetcode.cn/problems/pascals-triangle-ii/description/)
- [题解: 杨辉三角 II](https://leetcode.cn/problems/pascals-triangle-ii/solutions/2632497/er-wei-shu-zu-yang-hui-san-jiao-by-qing-fss6a/)

### 17.图片平滑器

- [原题地址: 图片平滑器](https://leetcode.cn/problems/image-smoother/description/)
- [题解: 图片平滑器](https://leetcode.cn/problems/image-smoother/solutions/2645818/shu-zu-tu-pian-ping-hua-qi-by-qing-yi-ru-f4ip/)

### 18.区间加法 II

- [原题地址: 区间加法 II](https://leetcode.cn/problems/range-addition-ii/description/)
- [题解: 区间加法 II](https://leetcode.cn/problems/range-addition-ii/post-solution/?submissionId=503196816)

### 19.甲板上的战舰

- [原题地址: 甲板上的战舰](https://leetcode.cn/problems/battleships-in-a-board/description/)
- [题解: 甲板上的战舰](https://leetcode.cn/problems/battleships-in-a-board/solutions/2647856/shu-zu-jia-ban-shang-de-zhan-jian-by-qin-u9l3/)

### 20.螺旋矩阵

- [原题地址: 螺旋矩阵](https://leetcode.cn/problems/spiral-matrix/description/)
- [题解: 螺旋矩阵](https://leetcode.cn/problems/spiral-matrix/solutions/2649351/shu-zu-luo-xuan-ju-zhen-by-qing-yi-ruo-s-d7lr/)

### 21.螺旋矩阵 II

- [原题地址: 螺旋矩阵 II](https://leetcode.cn/problems/spiral-matrix-ii/description/)
- [题解: 螺旋矩阵 II](https://leetcode.cn/problems/spiral-matrix-ii/solutions/2651403/shu-zu-luo-xuan-ju-zhen-ii-by-qing-yi-ru-oscx/)

### 22.对角线遍历
- [原题地址: 对角线遍历](https://leetcode.cn/problems/diagonal-traverse/description/)
- [题解: 对角线遍历](https://leetcode.cn/problems/diagonal-traverse/solutions/2654521/er-wei-shu-zu-dui-jiao-xian-bian-li-by-q-p1c8/)

### 23.重塑矩阵

- [原题地址: 重塑矩阵](https://leetcode.cn/problems/reshape-the-matrix/description/)
- [题解: 重塑矩阵](https://leetcode.cn/problems/reshape-the-matrix/solutions/2655593/er-wei-shu-zu-zhong-su-ju-zhen-by-qing-y-51vm/)

### 24.旋转图像

- [原题地址: 旋转图像](https://leetcode.cn/problems/rotate-image/description/)
- [题解: 旋转图像](https://leetcode.cn/problems/rotate-image/solutions/2658157/ju-zhen-xuan-zhuan-tu-xiang-by-qing-yi-r-zzh6/)

### 25.矩阵置零

- [原题地址: 矩阵置零](https://leetcode.cn/problems/set-matrix-zeroes/description/)
- [题解: 矩阵置零](https://leetcode.cn/problems/set-matrix-zeroes/solutions/2659672/ju-zhen-ju-zhen-zhi-ling-by-qing-yi-ruo-rk872/)
 
### 26.生命游戏

- [原题地址: 生命游戏](https://leetcode.cn/problems/game-of-life/description/)
- [题解: 生命游戏](https://leetcode.cn/problems/game-of-life/solutions/2661687/ju-zhen-sheng-ming-you-xi-by-qing-yi-ruo-s1pi/)
 
### 27.区域和检索

- [原题地址: 区域和检索](https://leetcode.cn/problems/range-sum-query-immutable/description/)
- [题解: 区域和检索](https://leetcode.cn/problems/range-sum-query-immutable/solutions/2666247/shu-zu-qu-yu-he-jian-suo-by-qing-yi-ruo-8091r/)
 
### 28.二维区域和检索 - 矩阵不可变 

- [原题地址: 二维区域和检索 - 矩阵不可变](https://leetcode.cn/problems/range-sum-query-2d-immutable/description/)
- [题解: 二维区域和检索 - 矩阵不可变](https://leetcode.cn/problems/range-sum-query-2d-immutable/solutions/2670223/ju-zhen-er-wei-qu-yu-he-jian-suo-ju-zhen-2vm1/)
- [题解: 区域和检索](https://leetcode.cn/problems/range-sum-query-immutable/solutions/2666247/shu-zu-qu-yu-he-jian-suo-by-qing-yi-ruo-8091r/)
 
### 29.除自身以外数组的乘积

- [原题地址: 除自身以外数组的乘积](https://leetcode.cn/problems/product-of-array-except-self/description/)
- [题解: 除自身以外数组的乘积](https://leetcode.cn/problems/product-of-array-except-self/solutions/2672774/qian-zhui-he-chu-zi-shen-yi-wai-shu-zu-d-r4zm/)

## 链表
 
### 30.设计链表

- [原题地址: 设计链表](https://leetcode.cn/problems/design-linked-list/description/)
- [题解: 设计链表](https://leetcode.cn/problems/design-linked-list/solutions/2684629/lian-biao-she-ji-lian-biao-by-qing-yi-ru-2924/)
 
### 31.移除链表元素

- [原题地址: 移除链表元素](https://leetcode.cn/problems/remove-linked-list-elements/description/)
- [题解: 移除链表元素](https://leetcode.cn/problems/remove-linked-list-elements/solutions/2686158/lian-biao-yi-chu-lian-biao-yuan-su-by-qi-tjus/)
 
### 32.删除链表中的节点

- [原题地址: 删除链表中的节点](https://leetcode.cn/problems/delete-node-in-a-linked-list/description/)
- [题解: 删除链表中的节点](https://leetcode.cn/problems/delete-node-in-a-linked-list/solutions/2688706/lian-biao-shan-chu-lian-biao-zhong-de-ji-4630/)

### 33.删除链表的倒数第 N 个结点

- [原题地址: 删除链表的倒数第 N 个结点](https://leetcode.cn/problems/remove-nth-node-from-end-of-list/description/)
- [题解: 删除链表的倒数第 N 个结点](https://leetcode.cn/problems/remove-nth-node-from-end-of-list/solutions/2694524/lian-biao-shan-chu-lian-biao-de-dao-shu-6w6xt/)

### 34.删除排序链表中的重复元素
   
- [原题地址: 删除排序链表中的重复元素](https://leetcode.cn/problems/remove-duplicates-from-sorted-list/description/)
- [题解: 删除排序链表中的重复元素](https://leetcode.cn/problems/remove-duplicates-from-sorted-list/solutions/2696133/lian-biao-shan-chu-pai-xu-lian-biao-zhon-mw58/)

### 35.删除排序链表中的重复元素 II

- [原题地址: 删除排序链表中的重复元素 II](https://leetcode.cn/problems/remove-duplicates-from-sorted-list-ii/description/)
- [题解: 删除排序链表中的重复元素 II](https://leetcode.cn/problems/remove-duplicates-from-sorted-list-ii/solutions/2706463/lian-biao-shan-chu-pai-xu-lian-biao-zhon-1hw6/)

### 36.扁平化多级双向链表

- [原题地址: 扁平化多级双向链表](https://leetcode.cn/problems/flatten-a-multilevel-doubly-linked-list/description/)
- [题解: 扁平化多级双向链表](https://leetcode.cn/problems/flatten-a-multilevel-doubly-linked-list/solutions/2709030/lian-biao-bian-ping-hua-duo-ji-shuang-xi-998r/)

## 字符串 

### 37.检测大写字母

- [原题地址: 扁平化多级双向链表](https://leetcode.cn/problems/detect-capital/description/)
- [题解: 扁平化多级双向链表](https://leetcode.cn/problems/detect-capital/solutions/2710761/zi-fu-chuan-jian-ce-da-xie-zi-mu-by-qing-t0sy/)

### 38.验证回文串

- [原题地址: 验证回文串](https://leetcode.cn/problems/valid-palindrome/description/)
- [题解: 验证回文串](https://leetcode.cn/problems/valid-palindrome/solutions/2746987/zi-fu-chuan-yan-zheng-hui-wen-chuan-by-q-frjx/)

### 39.最长公共前缀

- [原题地址: 最长公共前缀](https://leetcode.cn/problems/longest-common-prefix/description/)
- [题解: 最长公共前缀](https://leetcode.cn/problems/longest-common-prefix/solutions/2750609/zi-fu-chuan-zui-da-gong-gong-qian-zhui-b-gxx8/)

